var express = require('express');
var bodyParser = require('body-parser');
var PouchDB = require('pouchdb');
var fs = require('fs');
PouchDB.plugin(require('pouchdb-load'));

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'SyncServer' });
});

var parser = bodyParser.urlencoded({ extended: false });

router.post('/pouchdb_dump_file', parser, function(req, res){
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
	console.log(req.body);
  var dump_string = Object.keys(req.body)[0];

  // var dump_file_name = "dump.txt";
  // fs.writeFileSync(dump_file_name, dump_string);
  // data = fs.readFileSync(dump_file_name);
  // data_string = data.toString();

  db_name = 'user_sync1';

  var db = new PouchDB('http://jspblm:kingcouch13@couchdb.jspblm.com:5984/' + db_name);
  db.load(dump_string, {
      proxy: 'http://jspblm:kingcouch13@couchdb.jspblm.com:5984/' + db_name
    })
  	.then(function(res){
  			console.log("Response", res);
  		})
  	.catch(function(err){
  			console.log("Error", err);
  		});

  // var sync_process = db.sync('http://jspblm:kingcouch13@couchdb.jspblm.com:5984/' + db_name,
  // 	   {live: true,
  // 	   retry: true})
  //   	.on('change', function(info){
  //   		console.log('change', info);
  //   	})
  //   	.on('paused', function(){
  //   		console.log('paused, now cancel!');
  //       sync_process.cancel();
  //   	})
  //   	.on('active', function(){
  //   		console.log('active');
  //   	})
  //   	.on('denied', function(info){
  //   		console.log('denied', info);
  //   	})
  //   	.on('complete', function(info){
  //       // no se esta ejecutando
  //   		console.log('complete', info);
  //   	})
  //   	.on('error', function(err){
  //   		console.log('error', err);
  //   	});


	res.end('OK save');
});

module.exports = router;
